#!/usr/bin/env python3

from random import randrange
from colorama import init, deinit, Fore, Style


def display_board(board):
    # The function accepts one parameter containing the board's current status
    # and prints it out to the console.
    for cols in board:
        line = ("+"+"-"*7)*3+ "+"+"\n"
        line = line + ("|"+" "*7)*3+"|" +"\n"
        for r in cols:
            line = line + ("|"+" "*3)+str(r)+" "*3
        line = line + "|" + "\n"+ ("|"+" "*7)*3+"|"
        print(line)  
    print(("+"+"-"*7)*3+"+")
    
    

def enter_move(board):
    # The function accepts the board current status, asks the user about their move, 
    # checks the input and updates the board according to the user's decision.
    # check the input is appropriate
    try:
        move = int(input("Enter your move: "))
    except ValueError:
        print(Fore.RED+"You entered a non-compatible value.")
        print(Fore.RESET)
        board = enter_move(board)
        return board
    # escape condition
    if move==0:
        print(Fore.GREEN+"You entered 0, the game quits.")
        print(Fore.RESET)
        return False
    
    # get the board coordinates corresponding to the input
    move_r = ""
    move_c = ""
    for c in range(3):
        for r in range(3):
            if board[r][c] == move:
                move_r = r
                move_c = c
                break

    # check if the coordinates are in the free list
    free = make_list_of_free_fields(board)
    if (move_r,move_c) in free:
        board[move_r][move_c] = "O"
        display_board(board)
        if victory_for(board,"O"):
            print(Fore.GREEN+"The player won the game!")
            print(Fore.RESET)
            return False
        # if game not won check if there are still moves to perform
        else:
            availables = make_list_of_free_fields(board)
            if not availables:
                print(Fore.GREEN+"Draw, nobody wins!")
                print(Fore.RESET)
                board = False
            # in both cases return board wichi si false if there are no more moves otherwise is the board    
            return board
    else:
        print(Fore.RED+"The selected value is not available")    
        print(Fore.RESET)
        board = enter_move(board)
        return board
    


def make_list_of_free_fields(board):
    # The function browses the board and builds a list of all the free squares; 
    # the list consists of tuples, while each tuple is a pair of row and column numbers.
    available = []
    for c in range(3):
        for r in range(3):
            if isinstance(board[r][c], int):
                available.append((r,c)) 
    return available



def victory_for(board, sign):
    # The function analyzes the board status in order to check if 
    # the player using 'O's or 'X's has won the game
    tris = False
    # check the tris on the lines
    for row in range(3):
        if board[row][0]==board[row][1]==board[row][2]==sign:
            tris = tris or True
    # check the tris on the columns
    for col in range(3):
        if board[0][col]==board[1][col]==board[2][col]==sign:
            tris = tris or True
    # check the tris in the diagonals
    if board[0][0]==board[1][1]==board[2][2]==sign or \
        board[0][2]==board[1][1]==board[2][0]==sign:
            tris = tris or True
    return tris



def draw_move(board):
    # The function draws the computer's move and updates the board.
    available = make_list_of_free_fields(board)
    r = randrange(0,3)
    c = randrange(0,3)
    if (r,c) in available:
        board[r][c] = "X"
        display_board(board)
        # check if computer won
        if victory_for(board,"X"):
            print(Fore.RED+"The computer won the game!")
            print(Fore.RESET)
            return False
        # if not won check if there are still moves to perform
        else:
            availables = make_list_of_free_fields(board)
            if not availables:
                print(Fore.GREEN+"Draw, nobody wins!")
                print(Fore.RESET)
                board = False
            # in both cases return board wichi si false if there are no more moves otherwise is the board    
            return board
    # chosen place is not available on the board: repeat the function
    else:
        board = draw_move(board)
        return board


if __name__ == "__main__":
    # set graphic default
    init()
    print(Style.BRIGHT)
    print(Fore.CYAN)

    # initialize the board
    n = 1
    board = []
    for c in range(3):
        board.append([])
        for r in range(3):            
            board[-1].append(n)
            n+=1

    # iterate the process until the game ends or the player push 0 (board is False)
    while board:
        # computer moves first
        board = draw_move(board)
        if board:
            board = enter_move(board)
    print(Style.RESET_ALL)
    deinit()